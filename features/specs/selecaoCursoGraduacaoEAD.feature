#language: pt
@regressivo
Funcionalidade: Criar conta

@cadastro_valido_fixo_pf
Cenário: Selecionar curso de graduação da instituição Uniasselvi
Dado que acesso o site da instituição Uniasselvi
E    que seleciono o inscreva-se na graduação
Quando seleciono o continuar após informar todos os dados de interesse no formulario de seleção
Então devo validar as informações conforme os dados enviados para a instituição