

Quando('seleciono o continuar após informar todos os dados de interesse no formulario de seleção') do
     @graduacao_page = GraduacaoPage.new
     @graduacao_page.clicar_select_li_estado
     @graduacao_page.clicar_optin_selec_li_estado
     @graduacao_page.clicar_select_li_cidade
     @graduacao_page.clicar_optin_select_li_cidade
     @graduacao_page.clicar_select_li_curso
     @graduacao_page.clicar_optin_select_li_curso
     @graduacao_page.clicar_select_li_unidade
     @graduacao_page.clicar_optin_select_li_unidade
end

Então('devo validar as informações conforme os dados enviados para a instituição') do
     expect(@graduacao_page.get_text_title_curso_graduacao).to eq('Análise e Desenvolvimento de Sistemas')
end
