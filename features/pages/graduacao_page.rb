
class GraduacaoPage < SitePrism::Page

     element :select_li_estado, 'select[id="mkt-estado"]'
     element :select_li_cidade, 'select[id="mkt-cidade"]'
     element :select_li_curso, 'select[id="mkt-curso"]'
     element :select_li_unidade, 'select[id="mkt-unidade"]'

     element :optin_selec_li_estado, 'select option[value="am"]'
     element :optin_select_li_cidade, 'select option[value="manaus"]'
     element :optin_select_li_curso, 'select option[value="analise-e-desenvolvimento-de-sistemas"]'
     element :optin_select_li_unidade, 'select option[value="manaus-am-chapada"]'
     element :title_curso_graduacao, '#load-content > section > div.gray-section > div > div > div > h2'

     def clicar_select_li_estado
          select_li_estado.click
     end

     def clicar_select_li_cidade
          select_li_cidade.click
     end

     def clicar_select_li_curso
          select_li_curso.click
     end

     def clicar_select_li_unidade
          select_li_unidade.click
     end

     def clicar_optin_selec_li_estado
          wait_until_optin_selec_li_estado_visible
          optin_selec_li_estado.click
     end

     def clicar_optin_select_li_cidade
          wait_until_optin_select_li_cidade_visible
          optin_select_li_cidade.click
     end

     def clicar_optin_select_li_curso
          wait_until_optin_select_li_curso_visible
          optin_select_li_curso.click
     end

     def clicar_optin_select_li_unidade
          wait_until_optin_select_li_unidade_visible
          optin_select_li_unidade.click
     end

     def get_text_title_curso_graduacao
          title_curso_graduacao.text
     end


end