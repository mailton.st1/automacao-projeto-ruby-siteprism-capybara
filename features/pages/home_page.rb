class HomePage < SitePrism::Page
     set_url '/'

     element :btn_inscreva_se, '#menu > ul > li.highlight.dropdown > a'
     element :li_curso_graduacao, '#mkt-graduacao-inscrevase-1'

     def clicar_btn_inscreva_se
          btn_inscreva_se.click
     end

     def clicar_li_curso_graduacao
          li_curso_graduacao.click
     end


end